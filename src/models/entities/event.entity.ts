import { Asset } from '@models/entities/asset.entity'
import User from './user.entity'
import {
  BelongsTo,
  Column,
  CreatedAt,
  Default,
  ForeignKey,
  Model,
  PrimaryKey,
  Table,
  UpdatedAt,
} from 'sequelize-typescript'
import { EventType } from '@enum/sync.enum'

@Table({
  tableName: 'Events',
})
export default class Event extends Model<Event> {
  @PrimaryKey
  @Column
  id!: number

  @Column
  from: string

  @Column
  to: string

  @Column
  amount: number

  @Column
  price: number

  @Column
  tx_hash: string

  @Default(EventType.Minted)
  @Column
  event_type_id: number

  @CreatedAt
  @Column
  createdAt!: Date

  @UpdatedAt
  @Column
  updatedAt!: Date

  // @BelongsTo(() => Asset, 'asset_id')
  // asset!: Asset;
}

export { Event }
