import User from '@models/entities/user.entity';
import {
  BelongsTo,
  Column,
  CreatedAt,
  ForeignKey,
  HasMany,
  Model,
  PrimaryKey,
  Table,
  UpdatedAt,
} from 'sequelize-typescript';

@Table({
  tableName: 'follows',
})
export default class Follow extends Model<Follow> {
  @PrimaryKey
  @ForeignKey(() => User)
  @Column
  following_id!: number;

  @PrimaryKey
  @ForeignKey(() => User)
  @Column
  follower_id!: number;

  @CreatedAt
  @Column
  createdAt!: Date;

  @UpdatedAt
  @Column
  updatedAt!: Date;

  @BelongsTo(() => User, 'following_id')
  following!: User;

  @BelongsTo(() => User, 'follower_id')
  follower!: User;
}

export { Follow };
