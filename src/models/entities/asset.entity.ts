import Category from '@models/entities/category.entity'
import User from './user.entity'
import {
  BelongsTo,
  Column,
  CreatedAt,
  Default,
  ForeignKey,
  HasMany,
  Model,
  PrimaryKey,
  Table,
  Unique,
  UpdatedAt,
} from 'sequelize-typescript'
import AssetType from '@enum/asset.enum'
import StatusType from '@enum/status.enum'
import AssetLikes from './assetlikes.entity'
import Event from './event.entity'

@Table({
  tableName: 'assets',
})
export default class Asset extends Model<Asset> {
  @PrimaryKey
  @Column
  id!: number

  @Default('')
  @Column
  name!: string

  @Default('')
  @Column
  image!: string

  @Default('')
  @Column
  description!: string

  @Default('')
  @Column
  nft_address!: string

  @Column
  token_id!: number

  @Default(0)
  @Column
  amount!: number

  @ForeignKey(() => Category)
  @Column
  category_id!: number

  @ForeignKey(() => User)
  @Column
  creator_id: number

  @Column
  creator_address: string

  @Default(0)
  @Column
  edition!: number

  @Unique
  @Column
  ipfs!: string

  @Default('{}')
  @Column
  metadata!: string

  @Column
  unlockable_content!: string

  @Default(0)
  @Column
  num_of_like!: number

  @Default(0)
  @Column
  num_of_view!: number

  @Default(0)
  @Column
  num_of_edition!: number

  @Default(0)
  @Column
  num_of_claim!: number

  @Column
  is_sensitive: boolean

  @Column
  is_hidden: boolean

  @Default(AssetType.ERC721)
  @Column
  type!: number

  @Default(StatusType.LIST)
  @Column
  status!: number

  @CreatedAt
  @Column
  createdAt!: Date

  @UpdatedAt
  @Column
  updatedAt!: Date

  @BelongsTo(() => Category, 'category_id')
  category!: Category

  @BelongsTo(() => User, 'creator_id')
  creator!: User

  @HasMany(() => AssetLikes, 'asset_id')
  assetlikes!: AssetLikes[]

  // @HasMany(() => Event, 'asset_id')
  // events!: Event[];
}

export { Asset }
