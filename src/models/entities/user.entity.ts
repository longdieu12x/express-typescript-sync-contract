import { AssetLikes } from '@models/entities/assetlikes.entity';
import {
  Column,
  CreatedAt,
  HasMany,
  Model,
  PrimaryKey,
  Table,
  UpdatedAt,
} from 'sequelize-typescript';
import Asset from './asset.entity';
import Collection from './collection.entity';
import Follow from './follow.entity';

@Table({
  tableName: 'users',
})
export default class User extends Model<User> {
  @PrimaryKey
  @Column
  id!: number;

  @Column
  name!: string;

  @Column
  description!: string;

  @Column
  email!: string;

  @Column
  country_id: number;

  @Column
  address: string;

  @Column
  nonce: string;

  @Column
  num_of_followers: number;

  @Column
  num_of_followings: number;

  @Column
  profile_img: string;

  @Column
  banner_img: string;

  @Column
  facebook_url: string;

  @Column
  twitter_url: string;

  @Column
  instagram_url: string;

  @Column
  youtube_url: string;

  @Column
  language: string;

  @Column
  type_user: number;

  @CreatedAt
  @Column
  createdAt!: Date;

  @UpdatedAt
  @Column
  updatedAt!: Date;

  @HasMany(() => Collection, 'owner_id')
  collection!: Collection[];

  @HasMany(() => Asset, 'creator_id')
  assets!: Asset[];

  @HasMany(() => AssetLikes, 'user_id')
  assetlikes!: AssetLikes[];

  @HasMany(() => Follow, 'follower_id')
  followers!: Follow[];

  @HasMany(() => Follow, 'following_id')
  followings!: Follow[];
}

export { User };
