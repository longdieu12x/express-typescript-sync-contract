import { User } from '@models/entities/user.entity';
import {
  BelongsTo,
  Column,
  CreatedAt,
  Default,
  ForeignKey,
  HasMany,
  Model,
  PrimaryKey,
  Table,
  UpdatedAt,
} from 'sequelize-typescript';
import Asset from './asset.entity';

@Table({
  tableName: 'assetlikes',
})
export default class AssetLikes extends Model<AssetLikes> {
  @PrimaryKey
  @ForeignKey(() => User)
  @Column
  user_id!: number;

  @PrimaryKey
  @ForeignKey(() => Asset)
  @Column
  asset_id!: number;

  @Default(true)
  @Column
  active: boolean;

  @CreatedAt
  @Column
  createdAt!: Date;

  @UpdatedAt
  @Column
  updatedAt!: Date;

  @BelongsTo(() => User, 'user_id')
  user!: User;

  @BelongsTo(() => Asset, 'asset_id')
  asset!: Asset;
}

export { AssetLikes };
