import {
  Column,
  CreatedAt,
  HasMany,
  Model,
  PrimaryKey,
  Table,
  UpdatedAt,
} from 'sequelize-typescript';
import Asset from './asset.entity';

@Table({
  tableName: 'categories',
})
export default class Category extends Model<Category> {
  @PrimaryKey
  @Column
  id!: number;

  @Column
  name!: string;

  @CreatedAt
  @Column
  createdAt!: Date;

  @UpdatedAt
  @Column
  updatedAt!: Date;

  @HasMany(() => Asset, 'category_id')
  assets!: Asset[];
}

export { Category };
