import { ParsedUrlQueryInput } from 'querystring';

export interface IPagination extends ParsedUrlQueryInput {
  page: string;
  limit: string;
  order: string;
}
