export interface IRefreshToken {
  id: number;
}

export interface IAccessToken {
  address: string;
}
