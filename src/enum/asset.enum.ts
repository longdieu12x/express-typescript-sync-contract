enum AssetType {
  ERC721,
  ERC1155,
}

export default AssetType;
