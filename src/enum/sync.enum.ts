enum EventType {
  Import,
  Created,
  Minted,
  Listing,
  Transfer,
  CreateAuction,
  PlaceBid,
  CloseAuction,
}

export { EventType };
