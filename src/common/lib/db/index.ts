import Event from '@models/entities/event.entity'
import Follow from '@models/entities/follow.entity'
import AssetLikes from '@models/entities/assetlikes.entity'
import { ModelCtor } from 'sequelize-typescript'

import DB from '@models/index'
import User from '@models/entities/user.entity'
import Category from '@models/entities/category.entity'
import Asset from '@models/entities/asset.entity'

export function getModelFromTableName(tableName: string): ModelCtor | undefined {
  let item = undefined
  switch (tableName) {
    case User.tableName:
      item = DB.sequelize.model(User)
      break
    case Category.tableName:
      item = DB.sequelize.model(Category)
      break
    case Asset.tableName:
      item = DB.sequelize.model(Asset)
      break
    case AssetLikes.tableName:
      item = DB.sequelize.model(AssetLikes)
      break
    case Follow.tableName:
      item = DB.sequelize.model(Follow)
      break
    case Event.tableName:
      item = DB.sequelize.model(Event)
      break
    default:
      item = undefined
      break
  }
  return item
}
