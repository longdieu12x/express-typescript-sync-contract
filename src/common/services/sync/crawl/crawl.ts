import rpc from '@utils/rpc'
import { Service } from 'typedi'
import Block from '@utils/block'
import contract from '@utils/contract'
import { ethers } from 'ethers'
import Sync from '@utils/sync'
import eventType from '@utils/event'

@Service()
class Crawl {
  constructor(
    protected syncInstance: Sync,
    protected blockInstance: Block,
    protected provider = new ethers.providers.JsonRpcProvider(rpc.localhost),
  ) {}

  /**
   * @dev we will loop through start block to end block to check all event that we care
   */
  async start() {
    const whitelistAddresses = this.syncInstance.getAllSyncAddress()
    const whitelistInterface = this.syncInstance.getWhitelistInterface()

    const minBlock = await this.blockInstance.getMinimumBlock(
      Object.values(contract).map((item) => item.hash),
    )

    const currentBlock = await this.provider.getBlockNumber()

    const eventsList = await this.syncInstance.getSyncEvents(
      minBlock,
      currentBlock,
      this.provider,
      whitelistAddresses,
      whitelistInterface,
    )

    console.log(eventsList)
  }

  async setProvider(provider: ethers.providers.JsonRpcProvider) {
    this.provider = provider
  }
}

export default Crawl
