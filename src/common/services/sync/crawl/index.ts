import Crawl from './crawl';
import Container from 'typedi';
const main = async () => {
  const crawlInstance = Container.get(Crawl);
  await crawlInstance.start();
};

main();
