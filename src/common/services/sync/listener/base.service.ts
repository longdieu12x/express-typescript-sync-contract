import { providers, utils } from 'ethers';
import rpc from '@utils/rpc';

abstract class BaseListenService {
  protected address;
  protected event;
  protected abi;
  public rpc = rpc.localhost;
  constructor(address: string, event: string[], abi: string[]) {
    this.address = address;
    this.event = event;
    this.abi = abi;
  }

  async listen() {
    console.log('Listening...');
    const provider = new providers.JsonRpcProvider(this.rpc);
    const filter = {
      address: this.address,
      topics: [...this.event.map((item) => utils.id(item))],
    };
    const iface = new utils.Interface(this.abi);
    provider.on(filter, async (logs) => {
      await this.handleListener(iface.parseLog(logs));
    });
  }

  changeRpc(rpc: string) {
    this.rpc = rpc;
  }

  abstract handleListener(logs: utils.LogDescription);
}

export default BaseListenService;
