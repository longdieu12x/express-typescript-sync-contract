import BaseListenService from './base.service';
import { ethers } from 'ethers';

class ListingListenerService extends BaseListenService {
  async handleListener(logs: ethers.utils.LogDescription) {
    console.log('----------------------------------------');
    console.log(logs.name);
    console.log(logs.args);
  }
}

export default ListingListenerService;
