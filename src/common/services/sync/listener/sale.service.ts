import BaseListenService from './base.service';
import { ethers } from 'ethers';

class SaleListenerService extends BaseListenService {
  async handleListener(logs: ethers.utils.LogDescription) {
    console.log('----------------------------------------');
    console.log(logs.name);
    console.log(logs.args);
  }
}

export default SaleListenerService;
