import { Asset } from '@models/entities/asset.entity';
import { create } from 'ipfs-http-client';
import { env } from '@env';
let client = create({ url: 'http://ipfs.padisea.fun/api/v0' });

const uploadMetadata = async (data: Asset) => {
  const metadata = {
    name: data.name,
    description: data.description,
    image: data.image,
    attributes: JSON.parse(data.metadata),
  };
  const { cid } = await client.add(JSON.stringify(metadata));
  const ipfs = `${env.app.ipfs_url}/ipfs/${cid}`;
  return ipfs;
};

export { client, uploadMetadata };
