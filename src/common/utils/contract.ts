export default {
  padi721: {
    address: '0x2279B7A0a67DB372996a5FaB50D91eAA73d2eBe6',
    event: ['Mint(address,address,address,uint256,uint256,string)'],
    abi: [
      'event Mint(address from, address to, address contractNFT, uint256 tokenId, uint256 quantity, string ipfs)',
    ],
    hash: '0x582aea1e35b93bea620c215fc48b49eb5e7baa0b64edcac10b54054a1d6e5483',
  },
  padi1155: {
    address: '0x610178dA211FEF7D417bC0e6FeD39F05609AD788',
    event: ['Mint(address,address,address,uint256,uint256,string)'],
    abi: [
      'event Mint(address from, address to, address contractNFT, uint256 tokenId, uint256 quantity, string ipfs)',
    ],
    hash: '0x932dd60347f5f0b12dd1daafa1259bd2eca19bc3a75753bea0fed651dc94d281',
  },
  listing: {
    address: '0xDc64a140Aa3E981100a9becA4E685f962f0cF6C9',
    event: ['ListingItem(address,address,uint256,address,uint256,address,uint256)'],
    abi: [
      'event ListingItem(address from, address to, uint256 quantity, address contractNFT, uint256 tokenId, address contractERC20, uint256 price)',
    ],
    hash: '0x7b86441a3eaa6ac3d82af2d7d8be730e864e0c7b507bf1fd48fbe1738f0a9de3',
  },
  saleItem: {
    address: '0xDc64a140Aa3E981100a9becA4E685f962f0cF6C9',
    event: ['SaleItem(address,address,uint256,address,uint256,address,uint256)'],
    abi: [
      'event SaleItem(address from, address to, uint256 quantity, address contractNFT, uint256 tokenId, address contractERC20, uint256 price)',
    ],
    hash: '0x7b86441a3eaa6ac3d82af2d7d8be730e864e0c7b507bf1fd48fbe1738f0a9de3',
  },
};
