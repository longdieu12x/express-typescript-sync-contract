import { ethers } from 'ethers';
import { Service } from 'typedi';
import contract from './contract';
import event from './event';

@Service()
class Sync {
  constructor(protected contractUtils = contract, protected eventUtils = event) {}
  getAllSyncAddress(): string[] {
    const addresses = [];
    const contractValues = Object.values(this.contractUtils);
    for (let contractValue of contractValues) {
      addresses.push(contractValue.address);
    }
    return addresses;
  }

  getWhitelistEvents(): string[] {
    return this.eventUtils;
  }

  getWhitelistAbi(): string[][] {
    // Filter toan bo string abi de khong trung nhau
    const abi = new Set(Object.values(this.contractUtils).map((item) => item.abi[0]));
    // Tra ve lai mang abi theo yeu cau
    return [...abi].map((item) => [item]);
  }

  getWhitelistInterface(): ethers.utils.Interface[] {
    return this.getWhitelistAbi().map((item) => new ethers.utils.Interface(item));
  }

  checkEventExists(topic: string, iface: ethers.utils.Interface): boolean {
    try {
      iface.getEvent(topic);
      return true;
    } catch (error) {
      return false;
    }
  }

  async getSyncEvents(
    fromBlock: number,
    toBlock: number,
    provider: ethers.providers.JsonRpcProvider,
    whitelistAddresses: string[],
    whitelistInterface: ethers.utils.Interface[],
  ): Promise<{ string: ethers.utils.Result }[]> {
    const events = [];
    for (let idx = fromBlock; idx < toBlock; idx++) {
      const transactions = (await provider.getBlock(idx)).transactions;
      transactions.forEach(async (transaction) => {
        const txDetail = await provider.getTransactionReceipt(transaction);
        if (
          whitelistAddresses.includes(txDetail.to) ||
          whitelistAddresses.includes(txDetail.from)
        ) {
          const logs = txDetail.logs;
          logs.forEach((log) => {
            whitelistInterface.forEach((iface) => {
              if (this.checkEventExists(log.topics[0], iface)) {
                const name = iface.parseLog(log).name;
                const args = iface.parseLog(log).args;
                let obj = {};
                obj[name] = args;
                events.push(obj);
              }
            });
          });
        }
      });
    }
    return events;
  }
}

export default Sync;
