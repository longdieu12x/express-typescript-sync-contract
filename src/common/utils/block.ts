import rpc from '@utils/rpc';
import { ethers } from 'ethers';
import { Service } from 'typedi';

@Service()
class Block {
  async getMinimumBlock(txArray: string[]) {
    const provider = new ethers.providers.JsonRpcProvider(rpc.localhost);
    let minblock = Infinity;
    const transactionDetails = await Promise.all(
      txArray.map((item) => provider.getTransactionReceipt(item)),
    );
    transactionDetails.forEach((transactionDetail) => {
      if (minblock > transactionDetail.blockNumber) {
        minblock = transactionDetail.blockNumber;
      }
    });
    return minblock;
  }
}

export default Block;
