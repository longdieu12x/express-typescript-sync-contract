import { Response, NextFunction, Request } from 'express';
import { AuthRequest } from '@interfaces/response.interface';

function isOwnerAsset() {
  return (target: any, key: string, descriptor: any) => {
    const originalMethod = descriptor.value;
    descriptor.value = async function (req: AuthRequest, res: Response, next: NextFunction) {
      try {
        const { id } = req.params;
        const user = req.user;
        await this.assetRepository.checkAssetExist(parseInt(id), user.id);
        return originalMethod.apply(this, [req, res, next]);
      } catch (error) {
        return this.setStack(error.stack).setMessage('Error').responseErrors(res);
      }
    };
    return descriptor;
  };
}

export default isOwnerAsset;
