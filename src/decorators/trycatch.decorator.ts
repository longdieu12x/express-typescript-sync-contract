import { Response, NextFunction, Request } from 'express';
import { AuthRequest } from '@interfaces/response.interface';
function TryCatchDecorator() {
  return (target: any, key: string, descriptor: any) => {
    const originalMethod = descriptor.value;
    descriptor.value = function (req: Request | AuthRequest, res: Response, next: NextFunction) {
      try {
        return originalMethod.apply(this, [req, res, next]);
      } catch (error) {
        return this.setStack(error.stack).setMessage('Error').responseErrors(res);
      }
    };
    return descriptor;
  };
}
export default TryCatchDecorator;
