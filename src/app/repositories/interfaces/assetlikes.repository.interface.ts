import { Model } from 'sequelize';
import { BaseRepositoryInterface } from './base.repository.interface';

export interface AssetLikesRepositoryInterface<M extends Model> extends BaseRepositoryInterface {
  findOrCreate(asset_id: number, user_id: number): Promise<[M, boolean]>;
  findOneByCondition(asset_id: number, user_id: number): Promise<M>;
  countAllLikes(asset_id: number): Promise<number>;
  getAllLikes(asset_id: number): Promise<M[]>;
}
