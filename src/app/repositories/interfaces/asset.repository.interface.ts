import { Model } from 'sequelize';
import { BaseRepositoryInterface } from './base.repository.interface';

export interface AssetRepositoryInterface<M extends Model> extends BaseRepositoryInterface {
  checkAssetExist(asset_id: number, user_id: number): Promise<M>;
  freeze(asset_id: number, ipfs: string): Promise<[number]>;
  getAllByPagination(page: number, limit: number): Promise<{ rows: M[]; count: number }>;
  findByAssetId(asset_id: number): Promise<M>;
}
