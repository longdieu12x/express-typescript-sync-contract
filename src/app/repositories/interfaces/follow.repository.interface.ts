import { Model } from 'sequelize';
import { BaseRepositoryInterface } from './base.repository.interface';

export interface FollowRepositoryInterface<M extends Model> extends BaseRepositoryInterface {
  findOrCreateFollow(following_id: number, follower_id: number): Promise<[M, boolean]>;
}
