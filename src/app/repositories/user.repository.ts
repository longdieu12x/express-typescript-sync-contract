import User from '@models/entities/user.entity';
import { Service } from 'typedi';
import { ModelCtor } from 'sequelize-typescript';
import { BaseRepository } from './base.repository';
import { UserRepositoryInterface } from './interfaces/user.repository.interface';
import { ModelContainer } from '@decorators/model.decorator';
import { env } from '@env';

@Service({ global: true })
class UserRepository extends BaseRepository<User> implements UserRepositoryInterface<User> {
  constructor(@ModelContainer(User.tableName) User: ModelCtor<User>) {
    super(User);
  }

  async findByEmail(email: string): Promise<User> {
    return this.findByCondition({
      where: { email: email },
    });
  }

  async findOrCreateByAddress(address: string): Promise<[User, boolean]> {
    return this.findOrCreateByCondition({
      where: { address: address },
      raw: true,
      defaults: {
        address,
      },
    });
  }

  async findById(id: number): Promise<User> {
    return this.findByCondition({
      where: { id: id },
      raw: true,
      attributes: { exclude: ['nonce'] },
    });
  }

  async findByAddress(address: string): Promise<User> {
    return this.findByCondition({
      where: { address: address },
      raw: true,
    });
  }

  async updateNonce(address: string, nonce: string) {
    return await this.model.update(
      {
        nonce,
      },
      {
        where: {
          address,
        },
      },
    );
  }
}

export default UserRepository;
