import User from '@models/entities/user.entity';
import Follow from '@models/entities/follow.entity';
import { Service } from 'typedi';
import { ModelCtor } from 'sequelize-typescript';
import { BaseRepository } from './base.repository';
import { UserRepositoryInterface } from './interfaces/user.repository.interface';
import { ModelContainer } from '@decorators/model.decorator';
import { env } from '@env';
import { FollowRepositoryInterface } from './interfaces/follow.repository.interface';
import DB from '@models/index';

@Service({ global: true })
class FollowRepository extends BaseRepository<Follow> implements FollowRepositoryInterface<Follow> {
  constructor(@ModelContainer(Follow.tableName) Follow: ModelCtor<Follow>) {
    super(Follow);
  }

  async findOrCreateFollow(following_id: number, follower_id: number): Promise<[Follow, boolean]> {
    return await this.findOrCreateByCondition({
      where: {
        following_id,
        follower_id,
      },
      defaults: {
        following_id,
        follower_id,
      },
    });
  }

  async deleteFollow(following_id: number, follower_id: number) {
    return await this.model.destroy({
      where: {
        following_id,
        follower_id,
      },
    });
  }

  async getAllFollowings(follower_id: number) {
    return await this.model.findAndCountAll({
      where: {
        follower_id,
      },
      raw: true,
      attributes: { exclude: ['follower_id'] },
      include: [{ model: User, as: 'following' }],
    });
  }

  async getAllFollowers(following_id: number) {
    return await this.model.findAndCountAll({
      where: {
        following_id,
      },
      attributes: { exclude: ['following_id'] },
      include: [{ model: User, as: 'follower' }],
      raw: true,
    });
  }
}

export default FollowRepository;
