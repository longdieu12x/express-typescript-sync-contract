import Asset from '@models/entities/asset.entity';
import { Service } from 'typedi';
import { ModelCtor } from 'sequelize-typescript';
import { BaseRepository } from './base.repository';
import { ModelContainer } from '@decorators/model.decorator';
import { env } from '@env';
import { AssetRepositoryInterface } from './interfaces/asset.repository.interface';

@Service({ global: true })
class AssetRepository extends BaseRepository<Asset> implements AssetRepositoryInterface<Asset> {
  constructor(@ModelContainer(Asset.tableName) Asset: ModelCtor<Asset>) {
    super(Asset);
  }

  async checkAssetExist(asset_id: number, user_id: number): Promise<Asset> {
    return await this.model.findOne({
      where: {
        id: asset_id,
        creator_id: user_id,
      },
    });
  }

  async freeze(asset_id: number, ipfs: string) {
    return await this.model.update(
      {
        ipfs,
      },
      {
        where: {
          id: asset_id,
        },
      },
    );
  }

  async findByAssetId(asset_id: number) {
    return await this.model.findOne({
      where: {
        id: asset_id,
      },
    });
  }

  async getAllByPagination(page: number, limit: number) {
    const offset = (page - 1) * limit;
    return await this.model.findAndCountAll({
      offset,
      limit,
      raw: true,
    });
  }
}

export default AssetRepository;
