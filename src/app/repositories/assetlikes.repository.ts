import { User } from '@models/entities/user.entity';
import { Service } from 'typedi';
import { ModelCtor } from 'sequelize-typescript';
import { BaseRepository } from './base.repository';
import { ModelContainer } from '@decorators/model.decorator';
import { env } from '@env';
import AssetLikes from '@models/entities/assetlikes.entity';
import { AssetLikesRepositoryInterface } from './interfaces/assetlikes.repository.interface';

@Service({ global: true })
class AssetLikesRepository
  extends BaseRepository<AssetLikes>
  implements AssetLikesRepositoryInterface<AssetLikes>
{
  constructor(@ModelContainer(AssetLikes.tableName) AssetLikes: ModelCtor<AssetLikes>) {
    super(AssetLikes);
  }

  async findOrCreate(asset_id: number, user_id: number) {
    return await this.model.findOrCreate({
      where: {
        asset_id,
        user_id,
      },
      defaults: {
        asset_id,
        user_id,
        active: true,
      },
    });
  }

  async findOneByCondition(asset_id: number, user_id: number) {
    return await this.model.findOne({
      where: {
        asset_id,
        user_id,
      },
    });
  }

  async countAllLikes(asset_id: number) {
    return await this.model.count({
      where: {
        asset_id,
        active: true,
      },
    });
  }

  async getAllLikes(asset_id: number) {
    return await this.model.findAll({
      where: {
        asset_id,
        active: true,
      },
      include: [User],
    });
  }
}

export default AssetLikesRepository;
