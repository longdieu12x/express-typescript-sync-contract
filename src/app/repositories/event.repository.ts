import Event from '@models/entities/event.entity'
import { Service } from 'typedi'
import { ModelCtor } from 'sequelize-typescript'
import { BaseRepository } from './base.repository'
import { ModelContainer } from '@decorators/model.decorator'
import { env } from '@env'
import { EventRepositoryInterface } from './interfaces/event.repository.interface'
import DB from '@models/index'

@Service({ global: true })
class EventRepository extends BaseRepository<Event> implements EventRepositoryInterface<Event> {
  constructor(@ModelContainer(Event.tableName) Event: ModelCtor<Event>) {
    super(Event)
  }
  async createOnEventName(eventDto, eventName: string) {}
}

export default EventRepository
