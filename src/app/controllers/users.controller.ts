import AssetRepository from '@repositories/asset.repository';
import validationMiddleware from '@middlewares/validation.middleware';
import { UpdateProfileDto } from './../../dtos/user.dto';
import User from '@models/entities/user.entity';
import {
  Authorized,
  CurrentUser,
  Get,
  JsonController,
  Param,
  Put,
  Req,
  Res,
  UseBefore,
} from 'routing-controllers';
import { NextFunction, Response, Request } from 'express';
import { BaseController } from './base.controller';
import { Service } from 'typedi';
import UserRepository from '@repositories/user.repository';
import { AuthMiddleware } from '@middlewares/auth.middleware';
import { AuthRequest } from '@interfaces/response.interface';
import {
  plainToInstance,
  plainToClass,
  classToPlain,
  instanceToPlain,
  serialize,
} from 'class-transformer';
import TryCatchDecorator from '@decorators/trycatch.decorator';

@JsonController('/users')
@Service()
export class UsersController extends BaseController {
  constructor(protected userRepository: UserRepository) {
    super();
  }

  @TryCatchDecorator()
  @Get('/list')
  async getUser(@Req() req: any, @Res() res: any, next: NextFunction) {
    const findAllUsersData = await this.userRepository.getAll();
    return this.setData(findAllUsersData).setMessage('Success').responseSuccess(res);
  }

  @TryCatchDecorator()
  @Get('/nonce/:address')
  async getNonce(@Req() req: any, @Res() res: any, next: NextFunction) {
    const { address } = req.params;
    let nonce = '0';
    try {
      const data = await this.userRepository.findByAddress(address);
      nonce = data.nonce;
    } catch (err: any) {}
    return this.setData(nonce).setMessage('Success').responseSuccess(res);
  }

  @TryCatchDecorator()
  @Get('/detail/:id')
  async getUserDetail(@Req() req: Request, @Res() res: Response, next: NextFunction) {
    const { id } = req.params;
    const findUserById = await this.userRepository.findById(parseInt(id));
    return this.setData(findUserById).setMessage('Success').responseSuccess(res);
  }

  @Authorized()
  @UseBefore(AuthMiddleware)
  @UseBefore(validationMiddleware(UpdateProfileDto, 'body'))
  @TryCatchDecorator()
  @Put('/update')
  async updateProfile(@Req() req: AuthRequest, @Res() res: Response, next: NextFunction) {
    const user = req.user;
    const { id } = user;
    const data = plainToClass(UpdateProfileDto, req.body, { excludeExtraneousValues: true });
    await this.userRepository.update(
      {
        ...data,
      },
      { where: { id } },
    );
    return this.setMessage('Update profile successfully').responseSuccess(res);
  }
}

export default UsersController;
