import { NextFunction, Response, Request } from 'express';
import { LoginDto } from '../../dtos/auth.dto';
import UserRepository from '@repositories/user.repository';
import { BaseController } from './base.controller';
import { BadRequestError, Body, Get, JsonController, Post, Req, Res } from 'routing-controllers';
import { Service } from 'typedi';
import { setCacheExpire, getCacheExpire } from '@services/redis';
import { createAccessToken, createRefreshToken, verifyToken } from '@utils/token';
import { loginMessage } from '@utils/message';
import { ethers } from 'ethers';
import { REFRESH_TTL } from '@utils/constants';
import random from '@utils/random';
import { IAccessToken } from '@interfaces/token.interface';
import TryCatchDecorator from '@decorators/trycatch.decorator';

@JsonController('/auth')
@Service()
class AuthController extends BaseController {
  constructor(protected authRepository: UserRepository) {
    super();
  }

  @TryCatchDecorator()
  @Post('/login')
  async login(@Req() req: Request, @Res() res: Response, next: NextFunction) {
    const loginDto: LoginDto = req.body;
    const { address, signature } = loginDto;
    const data = await this.authRepository.findOrCreateByAddress(address);
    const user = data[0];
    // if (data[1] == true) {
    //   // Create a new collection for new user
    //   await this.authRepository.createCollection(`Collection #${data[0].id}`, data[0].id);
    // }
    let nonce = user.nonce;
    const message = loginMessage(address, nonce);
    const verifyAddress = ethers.utils.verifyMessage(message, signature);

    if (verifyAddress.toUpperCase() == address.toUpperCase()) {
      let nonce = random().toString();
      await this.authRepository.updateNonce(address, nonce);
      const accessToken = createAccessToken(user);
      const refreshToken = createRefreshToken(user);
      // Save to redis
      setCacheExpire(`auth_refresh_address_${address}`, refreshToken, REFRESH_TTL);

      return this.setData({
        accessToken,
        refreshToken,
      })
        .setCode(200)
        .setMessage('Success')
        .responseSuccess(res);
    } else {
      throw new BadRequestError('Wrong credentials');
    }
  }

  @TryCatchDecorator()
  @Post('/refresh')
  async refresh(@Req() req: Request, @Res() res: Response, next: NextFunction) {
    const { accessToken, refreshToken } = req.body;
    const decryptAccess = (await verifyToken(accessToken)) as IAccessToken;
    const address = decryptAccess.address;
    const oldRefresh = await getCacheExpire(`auth_refresh_address_${address}`);
    if (JSON.parse(oldRefresh?.toLowerCase() as string) == refreshToken.toLowerCase()) {
      const data = await this.authRepository.findByAddress(address);
      const user = data;
      const newAccessToken = createAccessToken(user);
      const newRefreshToken = createRefreshToken(user);

      // set refresh to redis
      setCacheExpire(`auth_refresh_address_${address}`, newRefreshToken, REFRESH_TTL);

      return this.setData({
        accessToken: newAccessToken,
        refreshToken: newRefreshToken,
      })
        .setCode(200)
        .setMessage('Success')
        .responseSuccess(res);
    } else {
      throw new BadRequestError('Wrong Tokens');
    }
  }
}

export default AuthController;
