import { IPagination } from './../../interfaces/pagination.interface';
import { AuthRequest } from './../../interfaces/response.interface';
import { AuthMiddleware } from './../middlewares/auth.middleware';
import { Response, Request, NextFunction } from 'express';
import { BaseController } from './base.controller';
import {
  Authorized,
  CurrentUser,
  Get,
  JsonController,
  Param,
  Put,
  Req,
  Patch,
  BadRequestError,
  Post,
  Res,
  UseBefore,
  QueryParam,
} from 'routing-controllers';
import AssetRepository from '@repositories/asset.repository';
import { CreateAssetDto } from 'dtos/asset.dto';
import { Service } from 'typedi';
import { uploadMetadata } from '@services/ipfs';
import { env } from '@env';
import validationMiddleware from '@middlewares/validation.middleware';
import TryCatchDecorator from '@decorators/trycatch.decorator';
import isOwnerAsset from '@decorators/asset.decorator';
import { ParsedUrlQuery, ParsedUrlQueryInput, parse } from 'querystring';
import AssetLikesRepository from '@repositories/assetlikes.repository';

@JsonController('/assets')
@Service()
class AssetController extends BaseController {
  constructor(
    protected assetRepository: AssetRepository,
    protected assetLikeRepository: AssetLikesRepository,
  ) {
    super();
  }

  @Authorized()
  @UseBefore(validationMiddleware(CreateAssetDto, 'body'))
  @UseBefore(AuthMiddleware)
  @TryCatchDecorator()
  @Post('/create')
  async create(@Req() req: AuthRequest, @Res() res: Response, next: NextFunction) {
    const assetDto: CreateAssetDto = req.body;
    const user = req.user;
    await this.assetRepository.create({
      ...assetDto,
      creator_id: user.id,
      creator_address: user.address,
    });
    return this.setData('Create asset successfully').setMessage('Success').responseSuccess(res);
  }

  @UseBefore(AuthMiddleware)
  @TryCatchDecorator()
  @isOwnerAsset()
  @Patch('/freeze/:id')
  async freeze(@Req() req: AuthRequest, @Res() res: Response, next: NextFunction) {
    const { id } = req.params;
    const data = await this.assetRepository.findByAssetId(parseInt(id));
    if (!data.ipfs) {
      const ipfs = await uploadMetadata(data);
      await this.assetRepository.freeze(parseInt(id), ipfs);
      return this.setMessage('Freeze asset successfully').responseSuccess(res);
    } else {
      return this.setMessage('Asset already frozen').responseSuccess(res);
    }
  }

  @TryCatchDecorator()
  @Get('/list')
  async getAllAssets(@Req() req: Request, @Res() res: Response, next: NextFunction) {
    let { page, limit } = req.query as IPagination;
    const findAllAssetsData = await this.assetRepository.getAllByPagination(
      parseInt(page),
      parseInt(limit),
    );
    return this.setData(findAllAssetsData).setMessage('Success').responseSuccess(res);
  }

  @TryCatchDecorator()
  @Patch('/view/:id')
  async viewAsset(@Req() req: Request, @Res() res: Response, next: NextFunction) {
    const { id } = req.params;
    const data = await this.assetRepository.findByAssetId(parseInt(id));
    data.num_of_view += 1;
    await data.save();
    return this.setMessage('View asset successfully').responseSuccess(res);
  }

  @Authorized()
  @UseBefore(AuthMiddleware)
  @TryCatchDecorator()
  @Patch('/like/:id')
  async likeAsset(@Req() req: AuthRequest, @Res() res: Response, next: NextFunction) {
    const { id } = req.params;
    const user = req.user;
    const data = await this.assetRepository.findByAssetId(parseInt(id));
    const [assetLikeData, isInit] = await this.assetLikeRepository.findOrCreate(
      parseInt(id),
      user.id,
    );
    if (!isInit && assetLikeData.active == true) {
      throw new BadRequestError('You have liked this asset!');
    }
    assetLikeData.active = true;
    data.num_of_like += 1;
    await Promise.all([assetLikeData.save(), data.save()]);
    return this.setMessage('Like asset successfully').responseSuccess(res);
  }

  @Authorized()
  @UseBefore(AuthMiddleware)
  @TryCatchDecorator()
  @Patch('/unlike/:id')
  async unlikeAsset(@Req() req: AuthRequest, @Res() res: Response, next: NextFunction) {
    const { id } = req.params;
    console.log(id);
    const user = req.user;
    const data = await this.assetRepository.findByAssetId(parseInt(id));
    const assetLikeData = await this.assetLikeRepository.findOneByCondition(parseInt(id), user.id);
    if (assetLikeData.active == false) {
      throw new BadRequestError('You have not liked this asset');
    }
    assetLikeData.active = false;
    data.num_of_like += 1;
    await Promise.all([assetLikeData.save(), data.save()]);

    return this.setMessage('Unlike asset successfully').responseSuccess(res);
  }

  @TryCatchDecorator()
  @Get('/like/:id')
  async getAllLikes(@Req() req: AuthRequest, @Res() res: Response, next: NextFunction) {
    const { id } = req.params;
    const totalLikes = await this.assetLikeRepository.getAllLikes(parseInt(id));
    return this.setData(totalLikes).setMessage('Success').responseSuccess(res);
  }
}

export default AssetController;
