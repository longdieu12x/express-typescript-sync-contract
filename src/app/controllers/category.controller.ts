import validationMiddleware from '@middlewares/validation.middleware';
import { CreateCategoryDto, UpdateCategoryDto } from './../../dtos/category.dto';
import { Request, Response, NextFunction } from 'express';
import {
  Authorized,
  Delete,
  Get,
  JsonController,
  Post,
  Put,
  Req,
  Res,
  UseBefore,
} from 'routing-controllers';
import { Service, Inject } from 'typedi';
import { BaseController } from './base.controller';
import CategoryRepository from '@repositories/category.repository';
import { AdminMiddleware } from '@middlewares/admin.middleware';
import TryCatchDecorator from '@decorators/trycatch.decorator';

@JsonController('/categories')
@Service()
class CategoryController extends BaseController {
  constructor(protected categoryRepository: CategoryRepository) {
    super();
  }

  @TryCatchDecorator()
  @Get('/list')
  async getAll(@Req() req: Request, @Res() res: Response, next: NextFunction) {
    const findAllCategoriesData = await this.categoryRepository.getAll();
    return this.setData(findAllCategoriesData).setMessage('Success').responseSuccess(res);
  }

  @Authorized()
  @UseBefore(AdminMiddleware)
  @UseBefore(validationMiddleware(CreateCategoryDto, 'body'))
  @TryCatchDecorator()
  @Post('/create')
  async create(@Req() req: Request, @Res() res: Response, next: NextFunction) {
    const data: CreateCategoryDto = req.body;
    await this.categoryRepository.create(data);
    return this.setMessage('Create category successfully').responseSuccess(res);
  }

  @Authorized()
  @UseBefore(AdminMiddleware)
  @TryCatchDecorator()
  @Put('/update')
  async update(@Req() req: Request, @Res() res: Response, next: NextFunction) {
    const data: UpdateCategoryDto = req.body;
    await this.categoryRepository.update(
      {
        name: data.name,
      },
      {
        where: {
          id: data.id,
        },
      },
    );
    return this.setMessage('Update category successfully').responseSuccess(res);
  }

  @Authorized()
  @UseBefore(AdminMiddleware)
  @TryCatchDecorator()
  @Delete('/delete/:id')
  async delete(@Req() req: Request, @Res() res: Response, next: NextFunction) {
    const { id } = req.params;
    await this.categoryRepository.deleteById(id);
    return this.setMessage('Delete category successfully').responseSuccess(res);
  }
}

export default CategoryController;
