import { AuthRequest } from './../../interfaces/response.interface';
import { BaseController } from './base.controller';
import { Request, Response, NextFunction } from 'express';
import { Service } from 'typedi';
import {
  Authorized,
  CurrentUser,
  Get,
  JsonController,
  Param,
  Put,
  Req,
  Patch,
  BadRequestError,
  Post,
  Res,
  UseBefore,
  Delete,
} from 'routing-controllers';
import FollowRepository from '@repositories/follow.repository';
import TryCatchDecorator from '@decorators/trycatch.decorator';
import { AuthMiddleware } from '@middlewares/auth.middleware';

@JsonController('/follows')
@Service()
class FollowController extends BaseController {
  constructor(protected followRepository: FollowRepository) {
    super();
  }

  @Authorized()
  @UseBefore(AuthMiddleware)
  @TryCatchDecorator()
  @Post('/follow/:id')
  async followUser(@Req() req: AuthRequest, @Res() res: Response, next: NextFunction) {
    const follower_id = req.user.id;
    const following_id = req.params.id;
    const [followData, isInit] = await this.followRepository.findOrCreateFollow(
      follower_id,
      parseInt(following_id),
    );
    if (!isInit) {
      throw new BadRequestError('You have followed this user');
    }
    return this.setMessage('Follow user successfully').responseSuccess(res);
  }

  @Authorized()
  @UseBefore(AuthMiddleware)
  @TryCatchDecorator()
  @Post('/unfollow/:id')
  async unfollowUser(@Req() req: AuthRequest, @Res() res: Response, next: NextFunction) {
    const follower_id = req.user.id;
    const following_id = req.params.id;
    await this.followRepository.deleteFollow(follower_id, parseInt(following_id));
    return this.setMessage('Unfollow user successfully').responseSuccess(res);
  }

  @Authorized()
  @UseBefore(AuthMiddleware)
  @TryCatchDecorator()
  @Get('/following')
  async getFollowings(@Req() req: AuthRequest, @Res() res: Response, next: NextFunction) {
    const follower_id = req.user.id;
    const followings = await this.followRepository.getAllFollowings(follower_id);
    return this.setData(followings).responseSuccess(res);
  }

  @Authorized()
  @UseBefore(AuthMiddleware)
  @TryCatchDecorator()
  @Get('/follower')
  async getFollowers(@Req() req: AuthRequest, @Res() res: Response, next: NextFunction) {
    const following_id = req.user.id;
    const followers = await this.followRepository.getAllFollowers(following_id);
    return this.setData(followers).responseSuccess(res);
  }
}

export default FollowController;
