import { AuthRequest } from './../../interfaces/response.interface';
import { BaseController } from './base.controller';
import { Request, Response, NextFunction } from 'express';
import { Service } from 'typedi';
import {
  Authorized,
  CurrentUser,
  Get,
  JsonController,
  Param,
  Put,
  Req,
  Patch,
  BadRequestError,
  Post,
  Res,
  UseBefore,
  Delete,
} from 'routing-controllers';
import EventRepository from '@repositories/event.repository';
import TryCatchDecorator from '@decorators/trycatch.decorator';
import { AuthMiddleware } from '@middlewares/auth.middleware';

@JsonController('/events')
@Service()
class EventController extends BaseController {
  constructor(protected eventRepository: EventRepository) {
    super();
  }
}

export default EventController;
