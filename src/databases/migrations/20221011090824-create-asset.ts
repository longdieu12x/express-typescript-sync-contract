module.exports = {
  up: async (QueryInterface, Sequelize) => {
    await QueryInterface.createTable('assets', {
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
      },
      name: {
        type: Sequelize.STRING,
        defaultValue: '',
        field: 'name',
      },
      image: {
        type: Sequelize.STRING,
        defaultValue: '',
        field: 'image',
      },
      description: {
        type: Sequelize.STRING,
        defaultValue: '',
        field: 'description',
      },
      external_link: {
        type: Sequelize.STRING,
        defaultValue: '',
        field: 'external_link',
      },
      nft_address: {
        type: Sequelize.STRING,
        defaultValue: '',
        field: 'nft_address',
      },
      token_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        field: 'token_id',
      },
      amount: {
        type: Sequelize.INTEGER,
        allowNull: true,
        field: 'amount',
      },
      category_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'CASCADE',
        onUpdate: 'RESTRICT',
        field: 'category_id',
        references: {
          model: 'categories',
          key: 'id',
        },
      },
      creator_id: {
        type: Sequelize.INTEGER,
        field: 'creator_id',
        allowNull: false,
        onDelete: 'CASCADE',
        onUpdate: 'RESTRICT',
        references: {
          model: 'users',
          key: 'id',
        },
      },
      creator_address: {
        type: Sequelize.STRING,
        field: 'creator_address',
        allowNull: false,
      },
      edition: {
        type: Sequelize.INTEGER,
        field: 'edition',
        defaultValue: 0,
      },
      ipfs: {
        type: Sequelize.STRING,
        field: 'ipfs',
        defaultValue: '',
        unique: true,
      },
      metadata: {
        type: Sequelize.STRING,
        field: 'metadata',
        defaultValue: '{}',
      },
      unlockable_content: {
        type: Sequelize.STRING,
        field: 'unlockable_content',
        defaultValue: '',
      },
      num_of_like: {
        type: Sequelize.INTEGER,
        field: 'num_of_like',
        defaultValue: 0,
      },
      num_of_view: {
        type: Sequelize.INTEGER,
        field: 'num_of_view',
        defaultValue: 0,
      },
      num_of_edition: {
        type: Sequelize.INTEGER,
        field: 'num_of_edition',
        defaultValue: 0,
      },
      num_of_claim: {
        type: Sequelize.INTEGER,
        field: 'num_of_claim',
        defaultValue: 0,
      },
      is_sensitive: {
        type: Sequelize.BOOLEAN,
        field: 'is_sensitive',
        defaultValue: false,
      },
      is_hidden: {
        type: Sequelize.BOOLEAN,
        field: 'is_hidden',
        defaultValue: false,
      },
      type: {
        type: Sequelize.INTEGER,
        field: 'type',
        defaultValue: 0,
      },
      status: {
        type: Sequelize.INTEGER,
        field: 'status',
        defaultValue: 0,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        field: 'created_at',
      },

      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        field: 'updated_at',
      },
    });
  },

  down: async (queryInterface) => queryInterface.dropTable('assets'),
};
