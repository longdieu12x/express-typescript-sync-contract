module.exports = {
  up: async (QueryInterface, Sequelize) => {
    await QueryInterface.createTable('follows', {
      following_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'CASCADE',
        onUpdate: 'RESTRICT',
        field: 'following_id',
        references: {
          model: 'users',
          key: 'id',
        },
      },

      follower_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'CASCADE',
        onUpdate: 'RESTRICT',
        field: 'follower_id',
        references: {
          model: 'users',
          key: 'id',
        },
      },

      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        field: 'created_at',
      },

      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        field: 'updated_at',
      },
    });
  },

  down: async (queryInterface) => queryInterface.dropTable('follows'),
};
