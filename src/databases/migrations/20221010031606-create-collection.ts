module.exports = {
  up: async (QueryInterface, Sequelize) => {
    await QueryInterface.createTable('collections', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },

      name: {
        type: Sequelize.STRING(255),
        field: 'name',
        allowNull: false,
      },

      user_id: {
        type: Sequelize.INTEGER,
        field: 'user_id',
        allowNull: true,
        onDelete: 'CASCADE',
        onUpdate: 'SET NULL',
        references: {
          model: 'users',
          key: 'id',
        },
      },

      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        field: 'created_at',
      },

      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        field: 'updated_at',
      },
    })
  },

  down: async (queryInterface) => queryInterface.dropTable('collections'),
}
