module.exports = {
  up: async (QueryInterface, Sequelize) => {
    await QueryInterface.createTable('users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      description: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      email: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      country_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      address: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      nonce: {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: '0',
      },
      num_of_followers: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      num_of_followings: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      profile_img: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      banner_img: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      facebook_url: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      twitter_url: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      youtube_url: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      instagram_url: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      language: {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: 'en',
      },
      type_user: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },

      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        field: 'created_at',
      },

      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        field: 'updated_at',
      },
    });

    await Promise.all([
      QueryInterface.addIndex('users', ['email'], {
        name: ['users', 'email', 'unique'].join('_'),
        indicesType: 'unique',
        type: 'unique',
      }),
    ]);
  },

  down: async (queryInterface) => queryInterface.dropTable('users'),
};
