module.exports = {
  up: async (QueryInterface, Sequelize) => {
    await QueryInterface.createTable('assetlikes', {
      user_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'CASCADE',
        onUpdate: 'RESTRICT',
        field: 'user_id',
        references: {
          model: 'users',
          key: 'id',
        },
      },

      asset_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'CASCADE',
        onUpdate: 'RESTRICT',
        field: 'asset_id',
        references: {
          model: 'assets',
          key: 'id',
        },
      },

      active: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
      },

      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        field: 'created_at',
      },

      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        field: 'updated_at',
      },
    });
  },

  down: async (queryInterface) => queryInterface.dropTable('assetlikes'),
};
