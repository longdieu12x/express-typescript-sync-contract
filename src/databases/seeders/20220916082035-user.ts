const wrapValuesWithDateTime = require('../utils/wrapValuesWithDateTime.ts');

const users = [
  {
    id: 2,
    name: 'name user 1',
    address: '0x1',
  },

  {
    id: 3,
    name: 'name user 2',
    address: '0x2',
  },

  {
    id: 4,
    name: 'name user 3',
    address: '0x3',
  },
];

module.exports = {
  async up(queryInterface) {
    return [await queryInterface.bulkInsert('users', wrapValuesWithDateTime(users))];
  },

  async down(queryInterface) {
    return [
      await queryInterface.bulkDelete('users', {
        id: users.map((collection) => collection.id),
      }),
    ];
  },
};
